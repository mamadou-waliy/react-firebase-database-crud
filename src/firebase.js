import firebase from "firebase";
import "firebase/database";

let config = {
  apiKey: "AIzaSyBiOav8mxeQv2RsDyEjzo6-wNNXpT2p0M4",
  authDomain: "bezkoder-firebase-1.firebaseapp.com",
  databaseURL: "https://bezkoder-firebase-1-default-rtdb.firebaseio.com/",
  projectId: "bezkoder-firebase-1",
  storageBucket: "bezkoder-firebase-1.appspot.com",
  messagingSenderId: "144338184002",
  appId: "1:144338184002:web:097859d36471c4b852cadb",
};

firebase.initializeApp(config);

export default firebase.database();